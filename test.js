const testResponse = [
  {
      state: 'success',
      errorCode: 'NO_STOCK'
  },
  {
    state: 'processing',
    errorCode: ''
  },
  {
      state: 'error',
      errorCode: 'null'
  },
  {
      state: 'error',
      errorCode: 'INCORRECT_DETAILS'
  },
  {
    errorCode: 'NO_STOCK'
  },
  {
      state: 'error',
      errorCode: 'null'
  },
  {
      state: 'error'
  }
];

let timer;

/**
* Gets the processing page
* @param {Object} Data - Oject from array index
*/
function getProcessingPage(data) {
  // SET OUTPUTS
  const responseErrors = {
  'noStock': { title: 'Error page', message: 'No stock has been found' },
  'incorrectDetails' : { title: 'Error page', message: 'Incorrect details have been entered' },
  'error': { title: 'Error page', message: null }
  };

  // SET OUTPUT RESPONSE FROM DATA INPUT OBJECT
  switch(data.state) {
    case 'success':
      return { title: 'Order complete', message: null };
    case 'processing':
      return {'waiting': 'waiting'};
    case 'error':
      if (data.errorCode === 'NO_STOCK') {
        return responseErrors.noStock
      } else if (data.errorCode === 'INCORRECT_DETAILS') {
        return  responseErrors.incorrectDetails;
      } else {
      return responseErrors.error;
    }
  default: 
    return responseErrors.error;
  }
}

/**
* Gets the array and loop
* @param {array} data
*/
function executeArray(testResponse) {
  // LOOP THRU ARRAY 
  if ((!Array.isArray(testResponse)) && !testResponse.length > 0 ) {
    console.log(`\n-------------------------------\n YOUR INPUT IS EITHER NOT AN ARRAY OR AN EMPTY ARRAY \n-------------------------------\n`);
    return;
  }

  testResponse.forEach(function(item) {
  let processedOutput;

  // CHECK TO SEE IF the property sate even exists
  if (!item.state) {
    console.log(`\n-------------------------------\n YOUR ARRAY INPUT DOESNT CONTAIN A CORE PROPERTY - state MOVING FORWARD \n-------------------------------\n`);
    return;
  }

  processedOutput = getProcessingPage(item);
    // PROCESSING PROPERTY IS FOUND
    if (processedOutput.waiting === 'waiting') {
      // WAIT FOR 2 SECONDS TO EXECUTE
      setTimeout(function() {
        console.log(`\n-------------------------------\n OUTPUT = WAITING \n-------------------------------\n`);
      }, 2000)
    } else {
      // CLEAR ANY TIMEOUT THAT COULD BE SET AND CARRY ON.
      clearTimeout();
      console.log(`\n-------------------------------\n OUTPUT = ${JSON.stringify(processedOutput)} \n-------------------------------\n`);
    }
  }); 
}

executeArray(testResponse);
