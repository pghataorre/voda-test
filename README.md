# README #

Vodafone coding challenge.
MUST have node.js installed


The code already eexecutes the following test array

`const testResponse = [
    	{
	      state: 'success',
	      errorCode: 'NO_STOCK'
		},
		{
		  state: 'processing',
		  errorCode: ''
		},
		{
	      state: 'error',
	      errorCode: 'null'
		},
		{
	      state: 'error',
	      errorCode: 'INCORRECT_DETAILS'
		},
		{
	      state: 'error',
	      errorCode: 'null'
		},
		{
	      state: 'error'
		}
	];`

for test purposes.

### What is this repository for? ###


 Creating a helper function that will be used to determine the output of an array of data.

  Each element of the array has the following structure:

    {
      state: <String> - a state to go to
      errorCode: <String> - optional error code
    }

  The states have different functionalities:

    'processing' = delay by 2 seconds, then fetch the next state
    'error' = handle the error code provided (see below)
    'success' = return from the helper with the object: { title: 'Order complete' message: null }

  Handling error codes:

    'NO_STOCK' = return from the helper with an object: { title: 'Error page', message: 'No stock has been found' }
    'INCORRECT_DETAILS' = return from the helper with an object: { title: 'Error page', message: 'Incorrect details have been entered' }
    null = return from the helper with an object: { title: 'Error page', message: null }
    undefined = return from the helper with an object: { title: 'Error page', message: null }

  Example usage:
  -------
  getProcessingPage([{ state: 'processing' }, { state: 'error' }])
  => should return after 2 seconds with the object: { title: 'Error page', message: null }

  Notes:
  - Provide the code and a description of how to run it

### How do I get set up? ###

* Clone the repository - run gthe command below on a command line.

git clone https://pghataorre@bitbucket.org/pghataorre/voda-test.git


* Navigate to the folder that holds the `test.js` file
* Run the following command on a command line.
  
  node test.js
  
